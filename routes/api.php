<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\VehiculoController;
use App\Http\Controllers\OrdenServicioController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group(['prefix' => 'auth'], function () {
//     Route::post('login', [AuthController::class, 'login']);
//     Route::post('register', [AuthController::class, 'register']);

//     Route::group(['middleware' => 'auth:api'], function() {
//       Route::get('logout', [AuthController::class, 'logout']);
//       Route::get('/user', [AuthController::class, 'user']);
//       Route::get('/refresh', [AuthController::class, 'refresh']);
//     });
// });

Route::group(['prefix' => 'auth'], function () {
  Route::post('login', [AuthController::class, 'login']);
  Route::post('register', [AuthController::class, 'register']);
  Route::post('guardarOrden', [OrdenServicioController::class, 'store']);

  Route::group(['middleware' => 'auth:api'], function() {
    Route::get('logout', [AuthController::class, 'logout']);
    Route::get('user', [AuthController::class, 'user']);
    Route::get('/refresh', [AuthController::class, 'refresh']);
    Route::get('/listarClienteCombo', [ClienteController::class, 'listarClienteCombo']);
    Route::post('/guardarCliente', [ClienteController::class, 'store']);
    Route::get('/listarItemCombo', [ItemController::class, 'listarItemCombo']);
    Route::get('/listarVehiculoCombo', [VehiculoController::class, 'listarVehiculoCombo']);
    Route::get('/listarTipoPersonaCombo', [ClienteController::class, 'listarTipoPersonaCombo']);
    
  });
});

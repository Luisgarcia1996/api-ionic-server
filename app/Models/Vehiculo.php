<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    use HasFactory;
    protected $table = 'vehiculos';
    public $timestamps = false;
    public function marcas()
    {
        return $this->belongsTo(Models/Marca::class,'marca_id');
    }
    public function colores()
    {
        return $this->belongsTo(Models/Color::class,'color_id');
    }
    public function tipo_vehiculos()
    {
        return $this->belongsTo(Models/TipoVehiculo::class,'tipo_vehiculo_id');
    }
    public function detalleOrdenServicios()
    { 
        return $this->hasMany(Models/DetalleOrdenServicio::class, 'foreign_key', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleOrdenServicio extends Model
{
    use HasFactory;
    protected $table = 'detalle_orden_servicios';
    protected $primaryKey = ['id','orden_servicio_id'];
    protected $fillable = ['vehiculo_id','servicio_id','precio','cantidad'];
    public $timestamps = false;
    public $incrementing = false;
    public function vehiculos()
    {
        return $this->belongsTo(Models/Vehiculo::class,'id');
    }
    public function servicios()
    {
        return $this->belongsTo(Models/Item::class,'id');
    }
}

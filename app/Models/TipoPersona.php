<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoPersona extends Model
{
    use HasFactory;
    protected $table = 'tipo_persona';
    public $timestamps = false;
    public function clientes()
    { 
        return $this->hasMany(Models/Cliente::class, 'foreign_key', 'tipo_persona_id');
    }
}

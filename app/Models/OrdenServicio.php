<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrdenServicio extends Model
{
    use HasFactory;
    protected $table = 'orden_servicios';
    protected $primaryKey = 'id';
    protected $fillable = ['fecha_orden_servicio','cliente_id','observacion_orden_servicio','orden_servicio_estado_id'];
    public $timestamps = false;

    public function detalleOrdenServicio()
    {
        return $this->hasMany(DetalleOrdenServicio::class,'orden_servicio_id')->select(['orden_servicio_id']);
    }
}

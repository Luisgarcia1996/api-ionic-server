<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoVehiculo extends Model
{
    use HasFactory;
    protected $table = 'tipo_vehiculos';
    public $timestamps = false;
    public function vehiculos()
    { 
        return $this->hasMany(Models/Vehiculo::class, 'foreign_key', 'id');
    }
}

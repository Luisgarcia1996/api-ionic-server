<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
     //
     protected $table = 'personas';
     protected $fillable = ['nombre','apellido','fecha_nacimiento','ruc_ci'];
     public $timestamps = false;
     public function getRouteKeyName()
     {
         return 'ruc_ci';
     }
     
     public function clientes()
     { 
         return $this->hasMany(Models/Cliente::class, 'foreign_key', 'id');
     }

}

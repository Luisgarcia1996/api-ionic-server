<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table = 'clientes';
    public $timestamps = false;
    public function personas()
    {
        return $this->belongsTo(Models/Persona::class,'id');
    }
    public function tipo_personas()
    {
        return $this->belongsTo(Models/TipoPersona::class,'tipo_persona_id');
    }
}

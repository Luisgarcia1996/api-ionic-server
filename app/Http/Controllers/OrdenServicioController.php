<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\OrdenServicio;
use App\Models\DetalleOrdenServicio;
class OrdenServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $success = false; //flag
        DB::beginTransaction();

        try {
           
            $orden_servicio = new OrdenServicio();

            $orden_servicio->fecha_orden_servicio = \Carbon\Carbon::createFromFormat('d/m/Y',  $request->input("fecha_orden_servicio"))
            ->format('Y-m-d');
            $orden_servicio->cliente_id =   $request->input("cliente_id");
            $orden_servicio->orden_servicio_estado_id = 1;
            $orden_servicio->observacion_ordern_servicio =  $request->input("observacion");
            $orden_servicio->save();
            $detalles = $request->datos;
           
            foreach ($detalles as $key => $filas) { 
                $detalle =  new DetalleOrdenServicio();
                $detalle->orden_servicio_id =  OrdenServicio::all()->last()->id;
                $detalle->vehiculo_id = $filas['vehiculo_id'];
                $detalle->servicio_id= $filas['servicio_id'];
                $detalle->cantidad = $filas['cantidad'];
                $detalle->precio = $filas['precio'];
                $detalle->save();
            
            }
         
            $success = true;
            if ($success) {
                DB::commit();
            }
          
        } catch (\Exception $e) {
            DB::rollback();
		    $success = false;
            return response()->json(["data" => $e->getMessage()], 500);
        }
        return response()->json(['message'=>'ORDEN CREADA CORRECTAMENTE'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

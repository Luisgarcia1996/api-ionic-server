<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Persona;
class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          //dd($request);
          $success = false; //flag
          DB::beginTransaction();
          $utimoId = 0;
          try {
             
              $persona = new Persona();
              $persona->nombre =   $request->input("nombre");
              $persona->apellido = $request->input("apellido");
              $persona->ruc_ci = $request->input("ruc_ci");
              $persona->save();
              $cliente = new Cliente();
              $cliente->ruc_ci = $request->input("ruc_ci");
              $cliente->tipo_persona_id = $request->input("tipo_persona_id");
              $cliente->dv = $request->input("dv");
              $cliente->persona_id = $persona->id;
              $cliente->save();
              $utimoId = $cliente->id;
              $success = true;
              if ($success) {
                  DB::commit();
              }
            
          } catch (\Exception $e) {
              DB::rollback();
              $success = false;
              return response()->json(["data" => $e->getMessage()], 500);
          }
          return response()->json(['id'=> $utimoId,'message'=>'CLIENTE INGRESADO CORRECTAMENTE'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function listarClienteCombo()
    {
        $clientes = DB::table('clientes')
            ->join('personas', 'clientes.persona_id', '=', 'personas.id')
            ->select('personas.*','clientes.id')
            ->get();
        $cliente = [];
        foreach ($clientes as $key => $value) {
            $cliente[] = ['value'=>$value->id ,"label"=>$value->nombre." ".$value->apellido];
        }
        return response()->json($cliente);
    }
    public function listarTipoPersonaCombo()
    {
        $tipo_personas = DB::table('tipo_persona')->get();
        $tipo_persona = [];
        foreach ($tipo_personas as $key => $value) {
            $tipo_persona[] = ['value'=>$value->tipo_persona_id ,"label"=>$value->nombre_tipo_persona];
        }
        return response()->json($tipo_persona);
    }
}
